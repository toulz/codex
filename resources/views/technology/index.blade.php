<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free bootstrap documentation template">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Codex</title>
    <!-- using online links -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- using local links -->
    <!-- <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css"> -->
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('css/theme/main.css')}}">
    <link rel="stylesheet" href="{{ asset('css/theme/sidebar-themes.css')}}">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
</head>

<body>
    <div class="page-wrapper toggled light-theme">
        {{-- <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <!-- sidebar-brand  -->
                <div class="sidebar-item sidebar-brand text-white font-weight-bold">Codex</div>
                <!-- sidebar-header  -->
                <!-- sidebar-search  -->
                <div class="sidebar-item sidebar-search">
                    <div>
                        <div class="input-group">
                            <input type="text" class="form-control search-menu" placeholder="Search...">
                            <div class="input-group-append"> <span class="input-group-text">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </span> </div>
                        </div>
                    </div>
                </div>

                <!-- sidebar-menu React Component -->
                <div id="navigation"></div>

                <!-- sidebar-menu  -->
            </div>
            <!-- sidebar-footer  -->
            <div class="sidebar-footer">
                <div class="dropdown">
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i> <span class="badge badge-pill badge-primary notification">1</span> </a>
                    <div class="dropdown-menu notifications" aria-labelledby="dropdownMenuMessage">
                        <div class="notifications-header"> <i class="fa fa-bell"></i> Notifications </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
                            <div class="notification-content">
                                <div class="icon"> <i class="fas fa-check text-success border border-success"></i> </div>
                                <div class="content">
                                    <div class="notification-detail">Download latest update </div>
                                    <div class="notification-time"> 20 minutes ago </div>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div> <a class="dropdown-item text-center" href="#">All notifications</a> </div>
                </div>
                <div>
                    <a id="pin-sidebar" href="#"> <i class="fa fa-power-off"></i> </a>
                </div>
                <div class="pinned-footer">
                    <a href="#"> <i class="fas fa-ellipsis-h"></i> </a>
                </div>
            </div>
        </nav> --}}
        <div id="app">Technology Index</div>
        <!-- page-content  -->
        <!-- page-content" -->
    </div>
    <!-- page-wrapper -->
    <script src="js/theme/prism.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="js/theme/main.js"></script>
</body>

</html>