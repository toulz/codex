<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free bootstrap documentation template">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Codex</title>
    <!-- using online links -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('css/theme/main.css')}}">
    <link rel="stylesheet" href="{{ asset('css/theme/sidebar-themes.css')}}">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
    <!-- semantic-ui-css -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css" />
</head>
<body>

    <!-- react app -->
    <div class="page-wrapper toggled light-theme">
        <div id="app"></div>
    </div>


    <script src="js/theme/prism.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="js/theme/main.js"></script>
</body>
</html>