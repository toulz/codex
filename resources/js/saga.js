import { call, put, takeEvery } from 'redux-saga/effects'
import axios from 'axios';

// worker Saga: will be fired on FETCH_TECHS_REQUEST actions
function* fetchTechs(action) {
    try {
        const techs = yield call([axios, axios.get], '/api/technologies');
        yield put({ type: "FETCH_TECHS_SUCCEEDED", payload: techs.data });  
    } catch (e) {
        yield put({ type: "FETCH_TECHS_FAILED", payload: e.message });
    }
}

function* storeTech(action) {
    try {
        const tech = yield call([axios, axios.post], '/api/technology', action.payload);
        const response = tech.data;
        if (response.hasOwnProperty('errors')) {
            console.log('store tech failed');
            console.log(response);
            yield put({ type: 'STORE_TECH_FAILED', payload: response });
        } else {
            yield put({ type: 'STORE_TECH_SUCCEEDED', payload: tech.data })
        }
    } catch(e) {
        yield put({ type: 'STORE_TECH_FAILED', payload: e.message });
    }
}

function* storeResource(action) {
    try {
        const resource = yield call([axios, axios.post], '/api/resource', action.payload);
        const response = resource.data;
        if (response.hasOwnProperty('errors')) {
            console.log('store resource failed');
            console.log(response);
            yield put({ type: 'STORE_RESOURCE_FAILED', payload: response });
        } else {
            yield put({ type: 'STORE_RESOURCE_SUCCEEDED', payload: response })
        }
    } catch(e) {
        yield put({ type: 'STORE_RESOURCE_FAILED', payload: e.message })
    }
}


// Create a Saga listener
function* codexSaga() {
    yield takeEvery('FETCH_TECHS_REQUEST', fetchTechs);
    yield takeEvery('STORE_TECH_REQUEST', storeTech);
    yield takeEvery('STORE_RESOURCE_REQUEST', storeResource);
}

export default codexSaga;