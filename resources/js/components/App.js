import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga'
import reducer from '../reducers/index';
import codexSaga from './../saga';
import Nav from '../containers/Nav';
import Technology from './Technology';
import Main from './Main';

const App = () => {
    return (
        <BrowserRouter>
            <Nav>
                <Switch>
                    <Route path='/technology/:tech' render={(props) => <Technology {...props} />} />
                    <Route component={Main} />
                </Switch>
            </Nav>
        </BrowserRouter>
    );
 }

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()
const middleware = applyMiddleware(logger, sagaMiddleware);

// Redux store
const store = createStore(reducer, middleware);
// run the saga
sagaMiddleware.run(codexSaga)

// populate technologies for initial status
store.dispatch({ type: 'FETCH_TECHS_REQUEST'});

// render App
if (document.getElementById('app')) {
    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider> 
        , document.getElementById('app')
    );
}












// Alternative way to pass initial data to react app

// First thing we need to do is create a function that will init our application 
// and expose it via window object(the function on itself would not be available 
// outside of React context due to how Webpack works):

// function runApplication(data, node) {
//     ReactDOM.render(<App data={data} />, node);
// }

//window.runReactApplication = runApplication;

// Now we can call our application at any given time:

{/* <div id="MyApp"></div>
<script src="react-application.js"></script>
<script>
    window.runReactApplication({
        user: {name: 'BTM', email: 'example@example.com' }
    }, document.querySelector('#MyApp'));
</script> */}