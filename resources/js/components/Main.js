import React from 'react';
import NavToggle from '../containers/NavToggle';
import RightAside from './../components/RightAside';
import { routeFormatter } from '../helpers/route';

const Main =  () => {

    return (
        <div>
            <main className="page-content">
                <div id="overlay" className="overlay"></div>
                <div className="container-fluid">

                    <NavToggle routeCrumbs={routeFormatter("Home")} />

                    <div className="row p-lg-4">
                        <article className="main-content col-md-9 pr-lg-5">
                            <h1 className="mb-2 mt-3">Main</h1>
                            <p className="lead mb-3"> How to get started with Docu! </p>
                            <p>Thank you for downloading our themes! We really appreciate it and ready to help you!</p>
                            <p> This guide will help you get started with the template! All the important stuff – compiling the source, file structure, build tools, file includes – is documented here, but should you have any questions, always feel free to reach out to info@sharebootstrap.com </p>
                            
                            {/* <!-- Seperator --> */}
                            <hr className="my-5" />
                            
                            {/* <!-- alert --> */}
                            <div className="alert alert-primary d-flex justify-content-start align-items-center" role="alert"> <span className="fa fa-info mr-4"></span>
                                <p className="m-0"> This documentation is always evolving. If you've not been here for a while, perhaps check out the This documentation is always evolving.This is a primary alert with <a href="#" className="alert-link">an example link</a>. Give it a click if you like. </p>
                            </div>
                            {/* <!-- heading --> */}
                            <h3 className="font-weight-bold mb-2">
                                Dev setup
                            </h3>
                            <p> To get started, you need to do the following: </p>
                            <ol>
                                <li><strong>Make sure you have Node installed</strong> since Landkit uses npm to manage dependencies. If you don't, installing is quite easy, just visit the <a href="https://nodejs.org/en/download/">Node Downloads page</a> and install it. </li>
                                <li><strong>Unzip your theme and open your command line</strong>, making sure your command line prompt is at the root of the unzipped theme directory. </li>
                                <li><strong className="badge badge-danger-soft"><code>npm install gulp-cli -g</code></strong>: If you don't have the Gulp command line interface, you need to install it.</li>
                                <li><strong className="badge badge-danger-soft"><code>npm install</code></strong>: Open your command line to the root directory of your unzipped theme and run to install all of Landkit's dependencies.</li>
                            </ol>
                            
                            {/* <!-- Sep --> */}
                            <hr className="my-5" />
                            <h3 className="font-weight-bold mb-2">
                                Code example
                            </h3>
                            <p>The Prism source, highlighted with Prism (don’t you just love how meta this is?):</p>
                            <p>The <a href="https://www.w3.org/TR/html5/grouping-content.html#the-pre-element">recommended way to mark up a code block</a> (both for semantics and for Prism) is a <code className=" language-markup"><span className="token tag"><span className="token tag"><span className="token punctuation">&lt;</span>pre</span><span className="token punctuation">&gt;</span></span></code> element with a <code className=" language-markup"><span className="token tag"><span className="token tag"><span className="token punctuation">&lt;</span>code</span><span className="token punctuation">&gt;</span></span></code> element inside, like so:</p>
                            <figure className="highlight"><pre><code className="language-html" data-lang="html"><span className="c">&lt;!-- As a link --&gt;</span>
                                <span className="nt">&lt;nav</span> <span className="na">className=</span><span className="s">"navbar navbar-light bg-light"</span><span className="nt">&gt;</span>
                                <span className="nt">&lt;a</span> <span className="na">className=</span><span className="s">"navbar-brand"</span> <span className="na">href=</span><span className="s">"#"</span><span className="nt">&gt;</span>Navbar<span className="nt">&lt;/a&gt;</span>
                                <span className="nt">&lt;/nav&gt;</span>

                                <span className="c">&lt;!-- As a heading --&gt;</span>
                                <span className="nt">&lt;nav</span> <span className="na">className=</span><span className="s">"navbar navbar-light bg-light"</span><span className="nt">&gt;</span>
                                <span className="nt">&lt;span</span> <span className="na">className=</span><span className="s">"navbar-brand mb-0 h1"</span><span className="nt">&gt;</span>Navbar<span className="nt">&lt;/span&gt;</span>
                                <span className="nt">&lt;/nav&gt;</span></code></pre>
                            </figure>
                            <hr className="my-5" />
                            <figure className="highlight"><pre><code className="language-html" data-lang="html"><span className="nt">&lt;nav</span> <span className="na">className=</span><span className="s">"navbar navbar-expand-lg navbar-light bg-light"</span><span className="nt">&gt;</span>
                                <span className="nt">&lt;div</span> <span className="na">className=</span><span className="s">"container"</span><span className="nt">&gt;</span>
                                <span className="nt">&lt;a</span> <span className="na">className=</span><span className="s">"navbar-brand"</span> <span className="na">href=</span><span className="s">"#"</span><span className="nt">&gt;</span>Navbar<span className="nt">&lt;/a&gt;</span>
                                <span className="nt">&lt;/div&gt;</span>
                                <span className="nt">&lt;/nav&gt;</span></code></pre>
                            </figure>
                        </article>

                        <RightAside />

                    </div>
                </div>
            </main>
        </div>
    );
}

export default Main;

