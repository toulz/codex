import React, {useState, useEffect} from 'react';
import NavToggle from '../containers/NavToggle';
import RightAside from './RightAside';
import { routeFormatter } from '../helpers/route';

const Technology = (props) => {

    const [tech, setTech] = useState(null);

    useEffect(() => {
        axios.get(`/api/technology/${props.match.params.tech}`)
            .then(res => {
                setTech(res.data);
            })
    }, [props.match.params.tech]);

    // create title, description and resources for the technology
    const techTitle = tech ? tech.title :
        <div className="spinner-grow text-primary" role="status">
            <span className="sr-only text-primary">Loading...</span>
        </div>

    const techDescription = tech ? tech.description : null;

    const resourceList = tech && tech.resources.length > 0 ? tech.resources.map(resource => (
        <div key={resource.id}>
            <a href={resource.url}><h6 className="pt-4">{resource.title}</h6></a>
            <p>{resource.description}</p>
        </div>
    )) : (
        <div className="text-muted">No resources added yet</div>
    )

    return (
        <main className="page-content">
            {/* TODO: may take this overlay out */}
            <div id="overlay" className="overlay"></div>

            <div className="container-fluid">
                <NavToggle routeCrumbs={ routeFormatter(props.location.pathname) } />
                <div className="row p-lg-4">
                    <div className="main-content col-md-9 pr-lg-5 mb-5">

                        <h1 className="mb-2 mt-3">{techTitle}</h1>
                        <p className="lead mb-3">{techDescription}</p>

                        <hr className="my-5" />
                        <h3 className="font-weight-bold mb-2">Resources</h3>
                        {resourceList}
                    </div>
                    <RightAside />
                </div>
            </div>

        </main>
    );
}

export default Technology;