import React from 'react';

export default () => (
    <div className="sidebar-item sidebar-search">
        <div>
            <div className="input-group">
                <input type="text" className="form-control search-menu" placeholder="Search..." />
                <div className="input-group-append">
                    <span className="input-group-text">
                        <i className="fa fa-search" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
)