import React from "react";

const RightAside = () => (
    <aside className="col-md-3 d-none d-md-block border-left">
        <div className="widget widget-support-forum p-md-3 p-sm-2"> <span className="icon icon-forum"></span>
            <h4>Looking for help? Join Community</h4>
            <p>Couldn’t find what your are looking for ? Why not join out support forums and let us help you.</p> <a href="#" className="btn btn-light">Support Forum</a>
        </div>
        <hr className="my-5" />
        <ul className="aside-nav nav flex-column">
            <li className="nav-item"> <a data-scroll="" className="nav-link text-dark" href="#section-1">Typography</a> </li>
            <li className="nav-item"> <a data-scroll="" className="nav-link text-dark" href="#section-3">File Tree</a> </li>
            <li className="nav-item"> <a data-scroll="" className="nav-link text-dark" href="#section-4">Table</a> </li>
            <li className="nav-item"> <a data-scroll="" className="nav-link text-dark" href="#section-5">Accordion</a> </li>
            <li className="nav-item"> <a data-scroll="" className="nav-link text-dark" href="#section-6">Video</a> </li>
            <li className="nav-item"> <a data-scroll="" className="nav-link text-dark" href="#section-9">List</a> </li>
            <li className="nav-item"> <a data-scroll="" className="nav-link text-dark" href="#section-10">Carousel</a> </li>
        </ul>
    </aside>
)

export default RightAside;