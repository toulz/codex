import React from 'react';

const SidebarFooter = () => {
    return (
        <div className="sidebar-footer">
            <div className="dropdown">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="fa fa-bell"></i> <span className="badge badge-pill badge-primary notification">1</span> </a>
                <div className="dropdown-menu notifications" aria-labelledby="dropdownMenuMessage">
                    <div className="notifications-header"> <i className="fa fa-bell"></i> Notifications </div>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" href="#">
                        <div className="notification-content">
                            <div className="icon"> <i className="fas fa-check text-success border border-success"></i> </div>
                            <div className="content">
                                <div className="notification-detail">Download latest update </div>
                                <div className="notification-time"> 20 minutes ago </div>
                            </div>
                        </div>
                    </a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item text-center" href="#">All notifications</a>
                </div>
            </div>
            <div>
                <a id="pin-sidebar" href="#"> <i className="fa fa-power-off"></i> </a>
            </div>
            <div className="pinned-footer">
                <a href="#"> <i className="fas fa-ellipsis-h"></i> </a>
            </div>
        </div>
    );
}

export default SidebarFooter;