
// Resource Constants
export const STORE_RESOURCE = 'STORE_RESOURCE';
export const STORE_RESOURCE_REQUEST = 'STORE_RESOURCE_REQUEST';
export const STORE_RESOURCE_SUCCEEDED = 'STORE_RESOURCE_SUCCEEDED';
export const STORE_RESOURCE_FAILED = 'STORE_RESOURCE_FAILED';
// export const FETCH_RESOURCES_REQUEST = 'FETCH_RESOURCES_REQUEST';
// export const FETCH_RESOURCES_SUCCEEDED = 'FETCH_RESOURCES_SUCCEEDED';
// export const FETCH_RESOURCES_FAILED = 'FETCH_RESOURCES_FAILED';


const initState = {
    storeResourceResponseMessages: [],
    storeResourceResponseType: ''
}

export const resource = (state = initState, action) => {
    if (typeof state === 'undefined') {
        return initState;
    }
    switch(action.type) {
        case STORE_RESOURCE_REQUEST:
            console.log('store resource dispatched');
            return state;
        case STORE_RESOURCE_SUCCEEDED:
            console.log('store resource succeeded');
            return Object.assign({}, state, {
                storeResourceResponseType: 'success',
                storeResourceResponseMessages: [action.payload.message]
            })
        case STORE_RESOURCE_FAILED:
            console.log('store resource failed from reducer');
            return Object.assign({}, state, {
                storeResourceResponseType: 'failed',
                storeResourceResponseMessages: [...Object.values(action.payload.errors)]
            });
        default:
            return state; 
    }
}
