
// Technology Constants
export const STORE_TECH_REQUEST = 'STORE_TECH_REQUEST';
export const STORE_TECH_SUCCEEDED = 'STORE_TECH_SUCCEEDED';
export const STORE_TECH_FAILED = 'STORE_TECH_FAILED';
export const FETCH_TECHS_REQUEST = 'FETCH_TECHS_REQUEST';
export const FETCH_TECHS_SUCCEEDED = 'FETCH_TECHS_SUCCEEDED';
export const FETCH_TECHS_FAILED = 'FETCH_TECHS_FAILED';

// Technology Action Creators
export const fetchTechsRequest = () => ({
    type: FETCH_TECHS_REQUEST
})

export const storeTech = techObj => ({
    type: STORE_TECH,
    payload: techObj
})

export const updateTech = techObj => ({
    type: UPDATE_TECH,
    payload: techObj
})

export const destroyTech = techObj => ({
    type: DESTROY_TECH,
    payload: techObj
})

// Initial State
const initState = {
    technologies: [],
    storeTechResponseMessages: [],
    storeTechResponseType: ''
}

export const tech = (state = initState, action) => {
    if (typeof state === 'undefined') {
        return initState;
    }
    switch(action.type) {
        case STORE_TECH_REQUEST:
            console.log('store tech dispatched');
            return state;
        case STORE_TECH_SUCCEEDED:
            console.log('store tech succeeded');
            return Object.assign({}, state, {
                technologies: [...state.technologies, action.payload.data],
                storeTechResponseType: 'success',
                storeTechResponseMessages: [action.payload.message]
            })
        case STORE_TECH_FAILED:
            console.log('store tech failed from reducer');
            return Object.assign({}, state, {
                storeTechResponseType: 'failed',
                storeTechResponseMessages: [...Object.values(action.payload.errors)]
            });
        case FETCH_TECHS_REQUEST:
            console.log('fetching technologies');
            return state;
        case FETCH_TECHS_SUCCEEDED:
            if (Array.isArray(action.payload)) {
                return Object.assign({}, state, {
                    technologies: action.payload
                })
            }
            return state;
        case FETCH_TECHS_FAILED:
            console.log(action.payload);
            return state;
        default:
            return state; 
    }
}

