import { tech } from './tech';
import { resource } from './resource';
import { combineReducers } from 'redux';

export default combineReducers({
    tech,
    resource
});