import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const SidebarMenu = ({ technologies }) => {

    // builds technology list
    const techList = technologies.length > 0 ? technologies.map(tech => (
        <li key={tech.id}>
            <Link to={`/technology/${tech.slug}`} id={tech.id}>
                {tech.title}
            </Link>
        </li>
    )) : (
            <li className="mx-auto">No Technology</li>
        );

    return (
        <div className=" sidebar-item sidebar-menu">
            <ul>
                <li className="sidebar-dropdown">
                    {/* TODO: change this link to a button */}
                    <a href="#">
                        <i className="fas fa-cogs"></i>
                        <span className="menu-text">Technologies</span>
                        <span className="badge badge-pill badge-primary">{technologies.length}</span>
                    </a>
                    <div className="sidebar-submenu">
                        <ul>
                            {techList}
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    )
}

const mapStateToProps = state => ({
    technologies: state.tech.technologies
})

export default connect(mapStateToProps)(SidebarMenu);