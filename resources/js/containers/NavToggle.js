import React from 'react';

const NavToggle = ({ routeCrumbs }) => {

    const breadCrumbListItems = routeCrumbs.map((crumb, index) => (
        <li className="breadcrumb-item active" aria-current="page">
            {crumb}
        </li>
    ));

    return (
        <div className="row d-flex align-items-center p-3 border-bottom">

            {/* toggle sidebar */}
            <div className="col-md-1">
                <a id="toggle-sidebar" className="btn rounded-0 p-3" href="#"> <i className="fas fa-bars"></i> </a>
            </div>

            {/* breadcrumbs */}
            <div className="col-md-8">
                <nav aria-label="breadcrumb" className="align-items-center">
                    <a href="index.html" className="breadcrumb-back" title="Back"></a>
                    <ol className="breadcrumb d-none d-lg-inline-flex m-0">
                        <li className="breadcrumb-item"></li>
                        { breadCrumbListItems}
                    </ol>
                </nav>
            </div>

        </div>
    );
}

export default NavToggle;