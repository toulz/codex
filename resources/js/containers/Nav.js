import React from 'react';
import SidebarSearch from './../components/SidebarSearch';
import SidebarMenu from './../containers/SidebarMenu';
import SidebarFooter from './../components/SidebarFooter';

const Nav = (props) => {
    return (
        <>
        <nav id="sidebar" className="sidebar-wrapper">
            <div className="sidebar-content">
                <div className="sidebar-item sidebar-brand text-white font-weight-bold">
                    <a href="/">Codex</a>
                </div>
                {/* may add search functionality later */}
                {/* <SidebarSearch /> */}
                <SidebarMenu />
            </div>
            <SidebarFooter />
        </nav>
        {props.children}
        </>
    );
}

export default Nav;

