/**
 * Formats a route path to an array to be passed to NavToggle component
 * eg. /technology/react is formatted to ["technology", "react"] 
 */
export const routeFormatter = (route) => {

    // if ( typeof route === 'string') {
    //     return route;
    // }

    return route.replace(/\//g, ' ')
        .trim()
        .split(' ');
}