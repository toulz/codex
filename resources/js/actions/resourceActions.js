
// Resource Constants
export const STORE_RESOURCE = 'STORE_RESOURCE';
export const UPDATE_RESOURCE = 'UPDATE_RESOURCE';
export const DESTROY_RESOURCE = 'DESTROY_RESOURCE';
export const FETCH_RESOURCE_REQUEST = 'FETCH_RESOURCE_REQUEST';
export const FETCH_RESOURCE_SUCCESS = 'FETCH_RESOURCE_SUCCESS';
export const FETCH_RESOURCE_FAILURE = 'FETCH_RESOURCE_FAILURE';

// Resource Action Creators
export const storeResource = resObj => ({
    type: 'STORE_RESOURCE',
    payload: resObj
})

export const storeResource = resObj => ({
    type: 'UPDATE_RESOURCE',
    payload: resObj
})

export const storeResource = resObj => ({
    type: 'DESTROY_RESOURCE',
    payload: resObj
})