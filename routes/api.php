<?php

use Illuminate\Http\Request;
use App\Technology;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    /**
     * Protected routes
     * 
     */
    Route::group(['middleware' => ['auth:api']], function() {
        
        Route::get('/user', function(Request $request) {
            return $request->user();
        });

        Route::get('/logout', 'Api\AuthController@logout')->name('logout');
    });


    /**
     * Public routes
     * 
     */
    Route::post('/login', 'Api\AuthController@login')->name('login.api');
    Route::post('/register', 'Api\AuthController@register')->name('register.api');



    /**
     * Technology Routes
     * 
     */
    Route::apiResource('technology', 'Api\TechnologyController')->except('index');

    Route::get('/technologies', function(Request $request) {
        $technologies = Technology::all();
        return $technologies; 
    });


    /**
     * Resource Routes
     * 
     */
    Route::apiResource('resource', 'Api\ResourceController');

