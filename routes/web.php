<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
});

// Auth::routes();

// Technology
// Route::resource('technology', 'TechnologyController');

// Fallback
Route::fallback(function() {
    return view('index');
});