<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Technology;

class Tutorial extends Model
{
    protected $connection = 'codex';
    protected $table = 'tutorials';
    protected $primayKey = 'id';
    public $incrementing = true;
    protected $fillable = ['technology_id', 'title', 'description', 'created_at', 'updated_at'];

    /**
     * Technology relationships
     * 
     */
    public function technology() {
        return $this->belongsTo(Technology::class, 'technology_id', 'id');
    }


}
