<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Resource;
use App\Tutorial;

class Technology extends Model
{
    protected $connection = 'codex';
    protected $table = 'technologies';
    protected $primayKey = 'id';
    public $incrementing = true;
    protected $fillable = ['slug', 'title', 'description', 'created_at', 'updated_at'];
    protected $with = ['resources'];
    
    /**
     * Resource relationships
     * 
     */
    public function resources() {
        return $this->hasMany(Resource::class, 'technology_id', 'id');
    }

    /**
     * Tutorial relationships
     * 
     */
    // public function tutorials() {
    //     return $this->hasMany(Tutorial::class, 'technology_id', 'id');
    // }

}
