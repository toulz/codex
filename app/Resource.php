<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Technology;

class Resource extends Model
{
    protected $connection = 'codex';
    protected $table = 'resources';
    protected $primayKey = 'id';
    public $incrementing = true;
    protected $fillable = ['technology_id', 'title', 'url', 'description', 'created_at', 'updated_at'];

    /**
     * Technology relationships
     * 
     */
    public function technology() {
        return $this->belongsTo(Technology::class, 'technology_id', 'id');
    }

}
