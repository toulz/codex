<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Carbon\Carbon;

class DatabaseBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup command for the Codex database';

    protected $process;

    protected $date;

    public function __construct()
    {
        parent::__construct();

        $this->date = Carbon::now()->format("Ymd");

        // Check if the backup file for today exists
        // If so, create a v2 file

        $this->process = new Process(sprintf(

            'mysqldump -u%s -p%s %s > %s',

            config('database.connections.codex.username'),

            config('database.connections.codex.password'),

            config('database.connections.codex.database'),

            storage_path("..\\..\\..\\Backups\\CodexBackup.{$this->date}.sql")

        ));

    }

    public function handle()
    {
        try {

            $this->process->mustRun();

            $this->info('');

            $this->info('The database backup was successful.');

            $this->info('');

            $this->info(

                sprintf(

                    "Backup file was stored at %s", 

                    storage_path("..\\..\\..\\Backups\\CodexBackup.{$this->date}.sql")

                )
            );

        } catch (ProcessFailedException $exception) {

            $this->error('The backup process has been failed.');

        }
    }
}
