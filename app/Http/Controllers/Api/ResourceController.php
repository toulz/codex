<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Resource;
use App\Technology;
use Illuminate\Http\Request;
use Validator;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $technology = Technology::find($request->technology);

        if (is_null($technology)) {
            return ['errors' => ['This technology does not exist.']];
        }
    
        $rules = [
            'url' => 'bail|required',
            'title' => 'bail|required',
            'description' => 'bail|required|string'
        ];

        $validator = Validator::make($request->except('technology'), $rules);
        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }

        $technology->resources()->create([
            'url' => $request->url,
            'title' => $request->title,
            'description' => $request->description,
        ]);
        return ['message' => 'Resource was successfully created.'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function show(Resource $resource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resource $resource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resource $resource)
    {
        //
    }
}
